package pdf_idr;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.BeforeClass;
import org.junit.Test;

import pdf_idr.PdfDataRetriever;

public class TestsPdfDataRetriever {
	static PdfDataRetriever ret;
	final static String path = "src/test/resources/InvTest.pdf";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {

	}

	@Test
	public void test() throws IOException {
		PdfDataRetriever ret= new PdfDataRetriever();
		assertEquals("CUIT: 27345932082",ret.getText(path));
	}

}
