package pdf_idr;

import java.io.File;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

import idr.core.extraction.Retriever;


public class PdfDataRetriever implements Retriever{

	@Override
	public String getCuitEmisor(File file) {
		return null;
	}

	@Override
	public String getCuitReceptor(File file) {
		return null;
	}
	
	/**
	 * Convierte el pdf a texto
	 * @param pdfPath
	 * @return
	 * @throws java.io.IOException 
	 */
	private String getText (String pdfPath) throws java.io.IOException{
		File pdfFile = new File(pdfPath);
	    PDDocument doc = PDDocument.load(pdfFile);
	    String result = new PDFTextStripper().getText(doc);
	    return result.trim();
	}
	
}
